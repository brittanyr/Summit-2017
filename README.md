Destination: Mexico!
TENTATIVE dates: January 10th - 17th 2017

More info to follow, but check out:

- Please check a box in our [SO poll](http://poll.fm/5ni1r) so we can make sure to have enough rooms :)

- [CDC Health information](http://wwwnc.cdc.gov/travel/destinations/traveler/none/mexico)
- [CDC info on the Zika virus](http://wwwnc.cdc.gov/travel/notices/alert/zika-virus-mexico)
    - Specific info for women who are (trying to get) pregnant: [here](http://www.cdc.gov/zika/pregnancy/thinking-about-pregnancy.html)
- [Visa info]( https://mexico.visahq.com/)
- Our [activities poll](http://poll.fm/5n080)
- Overview for getting visa, flights and briging your SO is in [this sheet](https://docs.google.com/spreadsheets/d/12MqT7aBHHGPes45Q9jTNyYRTTx9or7FPNTEl3HWJwoE/edit#gid=1198457175)